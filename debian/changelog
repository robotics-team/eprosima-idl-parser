eprosima-idl-parser (4.0.3+dfsg-1) unstable; urgency=medium

  * New upstream version 4.0.3+dfsg
  * Wrap and sort Debian package files

 -- Timo Röhling <roehling@debian.org>  Mon, 10 Feb 2025 22:02:37 +0100

eprosima-idl-parser (4.0.2+dfsg-1) unstable; urgency=medium

  * New upstream version 4.0.2+dfsg
  * Wrap and sort Debian package files

 -- Timo Röhling <roehling@debian.org>  Mon, 07 Oct 2024 15:05:41 +0200

eprosima-idl-parser (4.0.1+dfsg-1) unstable; urgency=medium

  * New upstream version 4.0.1+dfsg

 -- Timo Röhling <roehling@debian.org>  Sun, 15 Sep 2024 09:36:23 +0200

eprosima-idl-parser (4.0.0+dfsg-2) unstable; urgency=medium

  * Add Breaks for fastddsgen older than 4.0.0

 -- Timo Röhling <roehling@debian.org>  Thu, 29 Aug 2024 21:35:40 +0200

eprosima-idl-parser (4.0.0+dfsg-1) unstable; urgency=medium

  * New upstream version 4.0.0+dfsg
  * Refresh patches (no functional changes)

 -- Timo Röhling <roehling@debian.org>  Tue, 27 Aug 2024 15:37:19 +0200

eprosima-idl-parser (3.0.0+dfsg-2) unstable; urgency=medium

  * Add fallback for missing JavaScript engine

 -- Timo Röhling <roehling@debian.org>  Fri, 19 Apr 2024 13:05:57 +0200

eprosima-idl-parser (3.0.0+dfsg-1) unstable; urgency=medium

  * New upstream version 3.0.0+dfsg
  * Update patches
    - Drop Remove-import-of-Nullable.patch (obsolete)
    - Drop Fix-grammar-path-in-pom.xml.patch (merged upstream)
    - Drop Patch-StringTemplate-import-for-JDK-21.patch (obsolete)
    - Add Always-use-internal-JavaScript-engine.patch
  * Reformat d/control
  * Bump Standards-Version to 4.7.0
  * Update d/copyright

 -- Timo Röhling <roehling@debian.org>  Tue, 16 Apr 2024 20:21:01 +0200

eprosima-idl-parser (1.4.0+dfsg-2) unstable; urgency=medium

  [ Pushkar Kulkarni ]
  * Fix compilation failures with JDK 21

 -- Timo Röhling <roehling@debian.org>  Fri, 10 Nov 2023 11:54:07 +0100

eprosima-idl-parser (1.4.0+dfsg-1) unstable; urgency=medium

  * Update d/watch
  * New upstream version 1.4.0+dfsg

 -- Timo Röhling <roehling@debian.org>  Mon, 10 Oct 2022 21:50:08 +0200

eprosima-idl-parser (1.3.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.3.0+dfsg
  * Bump Standards-Version to 4.6.1

 -- Timo Röhling <roehling@debian.org>  Sat, 02 Jul 2022 14:37:18 +0200

eprosima-idl-parser (1.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.2.0+dfsg

 -- Timo Röhling <roehling@debian.org>  Sat, 19 Mar 2022 23:47:53 +0100

eprosima-idl-parser (1.1.2+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.2+dfsg

 -- Timo Röhling <roehling@debian.org>  Sun, 27 Feb 2022 13:53:42 +0100

eprosima-idl-parser (1.1.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.1+dfsg

 -- Timo Röhling <roehling@debian.org>  Sat, 25 Dec 2021 09:11:09 +0100

eprosima-idl-parser (1.1.0+dfsg-3) unstable; urgency=medium

  * Update Vcs-* URLs

 -- Timo Röhling <roehling@debian.org>  Sun, 10 Oct 2021 13:39:24 +0200

eprosima-idl-parser (1.1.0+dfsg-2) unstable; urgency=medium

  * Transfer package to new Debian Robotics Team
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Sat, 09 Oct 2021 12:02:08 +0200

eprosima-idl-parser (1.1.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.0+dfsg

 -- Timo Röhling <timo@gaussglocke.de>  Fri, 29 Jan 2021 12:08:18 +0100

eprosima-idl-parser (1.0.2+dfsg-2) unstable; urgency=medium

  * Source-only upload.

 -- Timo Röhling <timo@gaussglocke.de>  Mon, 07 Dec 2020 13:23:44 +0100

eprosima-idl-parser (1.0.2+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #976040)

 -- Timo Röhling <timo@gaussglocke.de>  Mon, 07 Dec 2020 13:23:09 +0100
